main(){

  double x=10.5;

  print(x);  //10.5

  num y=20.5;
  print(y);  //20.5
  print(y.runtimeType);  //double

  y=20;
  print(y);  //20
  print(y.runtimeType);   //int
   
  x=30;
  print(x);  //30.0
}