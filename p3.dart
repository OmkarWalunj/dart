main(){
  int x=10;
  print(x);  //10

  num y=20;
  print(y);  //20

  y=30.5;
  print(y);  //30.5

  x=30.5;    //Error: A value of type 'double' can't be assigned to a variable of type 'int'.
   print(x);   
}