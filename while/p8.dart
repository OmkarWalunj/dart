/*
Program 8: Write a program to print the countdown of days to
submit the assignment
Input : day = 7
Output: 7 days remaining
6 days remaining
5 days remaining
.
.
1 day remaining
0 days Assignment is Overdue
*/

void main(){

  int num=6;
  while(num>=0){
    if(num>0){
      print("$num days remaining");  
    }else{
      print("$num days Assignment is overdue.");
    }

    num--;

  }
}