/*
Program 2: Write a program to calculate the factorial of the
given number.
Input: 6
Output: factorial 6 is 720
*/

void main(){
  int i=6;
  int fact=1;
  while(i>=1){
    fact=fact*i;
    i--;
  }
  print(fact);
}