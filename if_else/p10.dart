/*Program 10 :
Write a dart program to calculate electricity bill of a house based
on following criteria
For first 90 units: No charge
90 to 180 units: 6 rupees per unit
180 to 250 units: 10 rupees per unit
Above 250 units : 15 rupees per unit
Input: 90
Output: 540
Input:120
Output:720
*/

void main(){

  var a=190;

  if(a<90){
    print("No charge");
  }else if(a>=90 && a<180){
    print(a*6);
  }else if(a>=180 && a<250){
    print(90*6+(a-180)*10);
  }else{
    print(90*6+(250-180)*10+(a-250)*15);
  }
}