/*Program 1:
Write a dart program to check if a number is even or odd.

Input: var=10;
Output: 10 is an even no
Input: var=37;
Output: 37 is an odd no
*/

void main() {
  var a=10;

  if(a % 2==0){
    print("$a is an even No");
  }else{
    print("$a is an odd No");
  }
}