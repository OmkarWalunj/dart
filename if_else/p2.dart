/*Program 2:
Write a dart program, take a number and print whether it is less
than 10 or greater than 10.
Input: var=5
Output: 5 Is Less than 10.
Input: var=16
Output: 16 Is greater than 10.*/

void main() {
  var a=10;

  if(a>10){
    print("$a is greater than 10");
  }else if(a==10) {
    print("$a is equal 10");
  }else{
    print("$a is less than 10");
  }
}