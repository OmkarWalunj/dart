/*Program 6 :
Write a dart program that takes a number from 0 to 5 and
prints its spelling, if the number is greater than 5 print
entered number is greater than 5
Input : var4= 4
Output : four*/

main(){

  var a=4;

  if(a==0){
    print("zero");
  }else if(a==1){
    print('one');
  }else if(a==2){
    print ('two');
  }else if(a==3){
    print ("three");
  } else if(a==4){
    print ("four");
  }else if(a<0){
    print("$a entered number is lesser then zero ");
  }else{
    print('$a entered number is greater than five ');
  }
}