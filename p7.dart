
//var

void main(){

  var x="Shashi";

  print(x);  //shashi
  print(x.runtimeType);  //String
 
  var y=30.5;
  print(y);  //30.5
  print(y.runtimeType);  //double

  x=20;  //Error: A value of type 'int' can't be assigned to a variable of type 'String'.
  print(x);
}